========================================
Project lists
========================================

IntegerDivider.java : The int only division program, supposed to output floating point numbers

========================================
Running
========================================

1. You should either clone or download the repository and a zip

2. Unzip the repo

3. Open the project in either IntelliJ or your favorite Java IDE

4. Compile and hope it works

5. Run it from the IDE or build into a jar

6. Profit.