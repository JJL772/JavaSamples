import java.util.*;

public class BitOperations
{
    public void Run(Scanner input)
    {
        System.out.println("Enter a number to perform a bitshift on:");
        int num = input.nextInt();
        System.out.println("Enter a number of bits to shift:");
        int shift = math.min(31, input.nextInt());
        shift = math.max(0, shift);
        System.out.println("You number has been shifted " + shift + " bits");
        System.out.println("The number result is: " + (num << shift));
    }
    
    public String GetProgramName()
    {
        return "Bit Operations";
    }
}