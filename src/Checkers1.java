import java.util.*;
import java.lang.*;

public class Checkers1 {

    //Main function
    public static void main(String[] args)
    {
        //Get input from the user
        String a,b;
        Scanner input = new Scanner(System.in);
        Board board = new Board();
        System.out.println("Hello, welcome to checkers.");
        System.out.println("Player 1 will be red and Player 2 will be black.");
        System.out.println("Player 1 please enter your name.");
        a = input.next();
        System.out.println("Player 2 please enter your name.");
        b = input.next();
        System.out.println("The game will now begin.");

        //Fill board. Too lazy for an actual algorithm. (if u want to do this use nested for loops and modulo for an iseven func)
        board.AddPiece(new Piece("red", false, false), 0, 1);
        board.AddPiece(new Piece("red", false, false), 0, 3);
        board.AddPiece(new Piece("red", false, false), 0, 5);
        board.AddPiece(new Piece("red", false, false), 0, 7);
        board.AddPiece(new Piece("red", false, false), 1, 0);
        board.AddPiece(new Piece("red", false, false), 1, 2);
        board.AddPiece(new Piece("red", false, false), 1, 4);
        board.AddPiece(new Piece("red", false, false), 1, 6);
        board.AddPiece(new Piece("red", false, false), 2, 1);
        board.AddPiece(new Piece("red", false, false), 2, 3);
        board.AddPiece(new Piece("red", false, false), 2, 5);
        board.AddPiece(new Piece("red", false, false), 2, 7);

        board.AddPiece(new Piece("black", false, false), 5, 0);
        board.AddPiece(new Piece("black", false, false), 5, 2);
        board.AddPiece(new Piece("black", false, false), 5, 4);
        board.AddPiece(new Piece("black", false, false), 5, 6);
        board.AddPiece(new Piece("black", false, false), 6, 1);
        board.AddPiece(new Piece("black", false, false), 6, 3);
        board.AddPiece(new Piece("black", false, false), 6, 5);
        board.AddPiece(new Piece("black", false, false), 6, 7);
        board.AddPiece(new Piece("black", false, false), 7, 0);
        board.AddPiece(new Piece("black", false, false), 7, 2);
        board.AddPiece(new Piece("black", false, false), 7, 4);
        board.AddPiece(new Piece("black", false, false), 7, 6);

        //print board
        board.Printboard();

        while(true) {
            //Check if the game is valid or not
            if (!board.IsGameValid())
                break;
            
            //Call to set kings
            board.HandleKings();

            //Ask player1 for move
            System.out.println("Player 1, would you like to move?");
            System.out.println("Please enter an X coordinate of the piece you want to move.");
            int x = input.nextInt();
            System.out.println("Please enter a Y coordinate of the piece you want to move.");
            int y = input.nextInt();

            //Using the two coords provided, try to get the object at that pos
            if (!board.board[y][x].ifempty && board.board[y][x].color.equals("red")) {
                //get piece. y and x are inverted because arrays work that way.
                Piece piece = board.board[y][x];

                //Loop until we get the proper x coord
                while (true) {
                    if (!board.IsGameValid())
                        break;
                    System.out.println("Please enter the X coordinate for the spot you would like to move to.");
                    int d = input.nextInt();
                    System.out.println("Please enter the y coordinate for the spot you would like to move to.");
                    int c = input.nextInt();
                    if (board.movepiece(piece, c, d)) {
                        break;
                    } else {
                        System.out.println("Invalid move");
                    }
                }
                
                //If game is over
                if (!board.IsGameValid())
                    break;

            } else {
                System.out.println("Invalid piece.");
            }
            System.out.println("Player 2, would you like to move?");
            System.out.println("Please enter an X coordinate of the piece you want to move.");
            x = input.nextInt();
            System.out.println("Please enter a Y coordinate of the piece you want to move.");
            y = input.nextInt();
            
            //messy indeed
            if (!board.board[y][x].ifempty && board.board[y][x].color.equals("red")) {
                Piece piece = board.board[y][x];
                while (true) {
                    if (!board.IsGameValid())
                        break;
                    
                    System.out.println("Please enter the X coordinate for the spot you would like to move to.");
                    int d = input.nextInt();
                    System.out.println("Please enter the y coordinate for the spot you would like to move to.");
                    int c = input.nextInt();
                    
                    if (board.movepiece(piece, c, d)) {
                        break;
                    } else {
                        System.out.println("Invalid move");
                    }
                }
            }
        }

    }
}

/**
 * This will represent a game piece.
 * Contains properties and stuff
 */
class Piece{
    /**
     * Default constructor. Used for compat and stuff
     */
    public Piece(){}

    /**
     * Our true constructor
     */
    public Piece(String color, boolean king, boolean empty ) {
        this.color = color;
        this.ifking = king;
        this.ifempty = empty;
        x = -1;
        y = -1;

        /*
         * Based on color, determine direction of piece
         */
        if(color == "black")
            diry = 1;
        else if(color == "red")
            diry = -1;
        else
            diry = 0;
    }

    //Member variables!!!!!!!
    public String color = "red";
    public boolean ifking = false;
    public boolean ifempty = false;
    public int x = -1;
    public int y = -1;
    public int diry = 0;

}

/**
 * Game board - Where all the game-related stuff will take place
 */
class Board{
    //The default empty piece. Saves CPU time by using a single object for filling the game board, instead of creating 1 per iteration
    private static final Piece emptypiece = new Piece("none",false,true);

    //Maintain a list of all game pieces and stuff
    public ArrayList<Piece> BlackPieces = new ArrayList<Piece>();
    public ArrayList<Piece> BlackPiecescollected = new ArrayList<Piece>();
    public ArrayList<Piece> RedPieces = new ArrayList<Piece>();
    public ArrayList<Piece> RedPiecescollected = new ArrayList<Piece>();

    //the game board
    public Piece [][] board = new Piece [8][8];

    //Constructor
    public Board()
    {
        //BE SURE WE POPULATE OR WE WILL GET A NULL POINTER EXCEPTION
        populate();
    }

    /**
     * Populates the board with empty pieces
     */
    public void populate(){
        //Loop through all parts of the board and fill with emptiness, like my soul
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                //Set each coord to the emptypiece constant
                board [x][y] = emptypiece;
            }

        }

    }

    /**
     * Returns the direction the piece is moving in
     */
    public int GetDirection(Piece pc, int x, int y)
    {
        //Determine the directions. Note: returns only -1, 1 or 0 for simplicity
        //If the move we are doing, minus the piece's position, is less than 1, we are moving down,
        //if its greater than 1, moving up and else, not moving vertically at all.
        if(x - pc.x < 0)
            return -1;
        else if(x - pc.x > 0)
            return 1;
        else
            return 0;
    }

    /**
     * Attempts to move a piece from one place to another
     * Returns false if unable, and true if able
     */
    public boolean movepiece(Piece piece, int x, int y)
    {

        //First we must check that the move is within the array's bounds
        if ( x < 8 && y < 8 && x >= 0 && y >= 0 ){
            //Record the distance between the piece and move site.
            double d = calcdistance(piece.x, piece.y, x, y);

            //If the piece is a king, execute a different snippet of code
            if(piece.ifking)
            {
                //If the distance is less than or equal to 1, we can try a move
                if(d <= 1 && board[x][y].ifempty)
                {
                    //Empty the slot we are moving from
                    board[piece.x][piece.y] = emptypiece;
                    //Move the piece to the new slot
                    board [x][y] = piece;
                    //Update the coords of the piece
                    piece.x = x;
                    piece.y = y;
                    //we're done! (for now)
                    return true;
                }
                //If the distance is less than or equal to 2, the move is going to be a jump
                else if(d <= 2 && board[x][y].ifempty)
                {
                    //Using some midpoint formula stuff, determine the x and y coords of the thing in the center
                    int sqx = piece.x + ((x - piece.x) / 2);
                    int sqy = piece.y + ((y - piece.y) / 2);

                    //If the piece at the jumpee's square is not of the user's square nor empty
                    if (!board[sqx][sqy].color.equals(piece.color) &&  !board[sqx][sqy].ifempty) {
                        //Perform our swap. Empty the piece's square
                        board[piece.x][piece.y] = emptypiece;
                        //Move the piece
                        board[x][y] = piece;

                        //Update the lists of pieces so remove pieces are gone
                        if (piece.color.equals("red")) {
                            //If it's red....
                            RedPiecescollected.add(board[sqx][sqy]);
                            BlackPieces.remove(board[sqx][sqy]);

                        } else {
                            //If it's black
                            BlackPiecescollected.add(board[sqx][sqy]);
                            BlackPieces.remove(board[sqx][sqy]);
                        }

                        //The jumpee's board square is now a void of emptiness
                        board[sqx][sqy] = emptypiece;

                        //Done again!
                        return true;
                    }
                    else
                    {
                        //In the event that we can't do those moves, we're screwed
                        return false;
                    }
                }
                else
                {
                    //if those moves are not possible.......
                    return false;
                }
            }

            //If it's not a king, this gets executed.
            //If distance is less than 1, and the square is empty, its a normal move
            if (d <= 1 && board[x][y].ifempty) {
                //Using GetDirection, determine if the piece can move where is wants
                //If the direction is 0 or the same as it's diry, it will move
                if(GetDirection(piece, x, y) == piece.diry || GetDirection(piece, x, y) == 0)
                {
                    //Perform the move, again
                    board[piece.x][piece.y] = emptypiece;
                    board [x][y] = piece;
                    //Update coords
                    piece.x = x;
                    piece.y = y;
                    //Done!
                    return true;
                }
                else
                {
                    //Else, invalid move, again
                    return false;
                }

            }
            //If the distance is <= 2, and the place we're moving to is empty
            else if(d <= 2 && board[x][y].ifempty) {
                //Midpoint formula again for middle square
                int sqx = piece.x + ((x - piece.x) / 2);
                int sqy = piece.y + ((y - piece.y) / 2);

                //If the place where the piece is jumping over has en enemy piece, and the direction is valid....
                if (!board[sqx][sqy].color.equals(piece.color) && (GetDirection(piece, x, y) == piece.diry || GetDirection(piece, x, y) == 0)) {
                    //Perform the swap for the move
                    board[piece.x][piece.y] = emptypiece;
                    board[x][y] = piece;

                    //Update the lists of pieces
                    if (piece.color.equals("red")) {
                        RedPiecescollected.add(board[sqx][sqy]);
                        BlackPieces.remove(board[sqx][sqy]);

                    } else {
                        BlackPiecescollected.add(board[sqx][sqy]);
                        BlackPieces.remove(board[sqx][sqy]);
                    }

                    //Jumped piece = null!
                    board[sqx][sqy] = emptypiece;

                    //Done again
                    return true;
                } else {
                    //Those are the only moves we have
                    return false;
                }
            }

        }
        //Return false; because if we break out of the if statement, we have other issues
        return false;
    }

    /**
     * Determines if the game is valid or not. Checks the list of pieces and returns false if one or more is empty.
     */
    public boolean IsGameValid()
    {
        if(RedPieces.isEmpty())
        {
            System.out.println("Player 2 (black) wins!");
            return false;
        }
        else if(RedPieces.isEmpty())
        {
            System.out.println("Player 1 (red) wins!");
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Handle the kings. Basically checks if a piece is king or not and updates it if in the right place
     */
    public void HandleKings(){
        //Loop through all x coords of the board
        for(int i = 0; i < 8; i++){
            //Checks if the space is empty or not
            if(!board[0][i].ifempty && !board[0][i].ifking)
            {
                //black becomes king at the top, and red the opposite
                if(board[0][i].color.equals("black")){
                    board[0][i].ifking = true;
                }
            }
            //Same as above
            else if(!board[7][i].ifempty && !board[7][i].ifking)
            {
                //red becomes king at the bottom, and black the opposite
                if(board[7][i].color.equals("red")){
                    board[7][i].ifking = true;
                }
            }
        }
    }

    /**
     * Real cancer where we print the board. DONT TOUCH PLEASE
     */
    public void Printboard(){
        //result string (result it stored here. Must be initialized
        String result = "";
        System.out.println("    0  1  2  3  4  5  6  7");
        //basically a loop. pretty cancer.
        //String.concat is used to stick two strings together. nuff said
        for(int x = 0; x < 8; x++){
            String line = x + "| ";
            for(int y = 0; y < 8; y++){
                if (board[x][y].ifempty) {
                    line = line.concat("[ ]");
                }
                else if(board[x][y].color.equals("red")){
                    if (board[x][y].ifking){
                        line = line.concat("[R]");
                    }
                    else{
                        line = line.concat("[r]");
                    }
                }
                else if(board[x][y].color.equals("black")){
                    if (board[x][y].ifking){
                        line = line.concat("[B]");
                    }
                    else{
                        line = line.concat("[b]");
                    }
                }
            }
            line = line.concat(" |\n");
            result = result.concat(line);
        }
        System.out.println(result);
    }

    /**
     * Uses the distance formula to calculate distance between two points
     */
    public double calcdistance(double x1, double x2, double y1, double y2){

        return Math.sqrt(Math.pow((x2-x1), 2 ) + Math.pow((y2-y1), 2));
    }

    /**
     * Adds a piece to the specified position
     */
    public void AddPiece(Piece pc, int x, int y)
    {
        if(pc.color == "red")
        {
            board[x][y] = pc;
            RedPieces.add(pc);
        }
        else
        {
            board[x][y] = pc;
            BlackPieces.add(pc);
        }
    }

}




