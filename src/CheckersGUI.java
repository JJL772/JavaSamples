/*
If you're reading this, you've just opened a stupid checkers game!
This file is quite large, as the game is completely contained withing this file.

This was written by: Jeremy L, who IS NOT AN AP CS STUDENT!!! SO IF YOU TURN THIS
IN FOR A FREAKING GRADE I WILL FIND YOU AND END YOU

Hope you learn something from this :D
*/

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;


enum ECheckersColor
{
    black,
    red,
}

/*
A checkers piece
 */
class CheckersPiece
{
    public boolean isKing;
    public final ECheckersColor Color;

    public CheckersPiece(ECheckersColor color) { Color = color; isKing = true; }
    public boolean IsKing() { return isKing; }
}

/*
A square in the game board
 */
class BoardSquare
{
    private boolean Empty;

    //Note: DO NOT ASSUME NON-NULL VALUE. THIS IS SET TO NULL WHEN THERE'S NOTHING IN THE SQUARE
    //AHHHHHHHH DONT CAUSE NULL POINTER EXCEPTIONS
    private CheckersPiece CurrentPiece;

    public BoardSquare()
    {
        Empty = true;
        CurrentPiece = null;
    }

    public void setPiece(CheckersPiece pc)
    {
        if(pc != null)
        {
            CurrentPiece = pc;
            Empty = false;
        }
        else
        {
            CurrentPiece = pc;
            Empty = true;
        }
    }

    public void empty()
    {
        CurrentPiece = null;
        Empty = true;
    }

    //DANGER DANGER: COULD BE NULL IF EMPTY
    public CheckersPiece getCurrentPiece()
    {
        return CurrentPiece;
    }

    public boolean isEmpty()
    {
        return Empty;
    }
}

/*
A player of our game
 */
class Player
{
    public String Username;

    //array of game pieces
    private CheckersPiece[] GamePieces;
    //array of collected pieces
    private CheckersPiece[] EnemyPieces;

    public final ECheckersColor PlayerColor;

    public Player(String name, ECheckersColor color)
    {
        Username = name;
        //Who actually knows how many pieces we will have?
        //This is here just so we dont have any null pointer exceptions
        GamePieces = new CheckersPiece[16];
        EnemyPieces = new CheckersPiece[16];
        PlayerColor = color;
    }

    public int getCollectedPieces()
    {
        if(EnemyPieces != null)
            return EnemyPieces.length;
        return 0;
    }

    public int getPieces()
    {
        if(GamePieces != null)
            return GamePieces.length;
        return 0;
    }

    public void addCollectedPiece(CheckersPiece piece)
    {
        EnemyPieces[EnemyPieces.length] = piece;
    }

    public void addPiece(CheckersPiece piece)
    {
        GamePieces[GamePieces.length] = piece;
    }
}

class CheckersBoard
{
    private final int Width;

    private final int Height;

    private BoardSquare[][] GameBoard;

    public CheckersBoard(int width, int height)
    {
        Width = width;
        Height = height;
        GameBoard = new BoardSquare[Width][Height];
    }

    BoardSquare[][] getGameBoard()
    {
        return GameBoard;
    }

    CheckersPiece getAt(int x, int y)
    {
        if(x <= Width && y <= Height)
        {
            return GameBoard[x][y].getCurrentPiece();
        }
        return null;
    }

    void setAt(int x, int y, CheckersPiece piece)
    {
        if(x <= Width && y <= Height)
        {
            GameBoard[x][y].setPiece(piece);
        }
    }
}

/*
Please ignore, these are listeners for menu items, which you dont need
 */
class Menu_ExitGameListener implements ActionListener
{
    public void actionPerformed(ActionEvent ev)
    {
        System.exit(0);
    }
}

/*
Name: Checkers

Purpose: The actual checkers game... Instance this for a dumb checkers game...

*/
public final class Checkers
{
    /*
    Gameboard used to hold all our stuff
     */
    private CheckersBoard GameBoard;

    /*
    Users
     */
    private Player User1;
    private Player User2;

    /*
    Time user movements, so we can tell them how slow they are.
     */
    private long MoveTime1;
    private long MoveTime2;

    private Scanner Input;

    private Frame GameWindow;

    private int PlayerTurn;

    /*
    Ctor
     */
    public Checkers()
    {
        //Dont forget to init!
        GameBoard = new CheckersBoard(8, 8);
        Input = new Scanner(System.in);

        //BEGIN MAGIC
        SetupGUI();
        //END MAGIC
    }

    /*
    Here's where all the GUI stuff gets set up
     */
    public void SetupGUI()
    {
        GameWindow = new Frame("Checkers");
        GameWindow.setBounds(Toolkit.getDefaultToolkit().getScreenSize().width / 2, Toolkit.getDefaultToolkit().getScreenSize(). height / 2,
                720, 480);
        GameWindow.setBackground(Color.black);
        GameWindow.setVisible(true);
        MenuBar menu = new MenuBar();
        Menu item = new Menu();
        item.setName("File");
        item.setLabel("File");
        MenuItem exitGame = new MenuItem();
        exitGame.addActionListener(new Menu_ExitGameListener());
        exitGame.setLabel("Exit");
        item.add(exitGame);
        menu.add(item);
        GameWindow.setMenuBar(menu);
    }

    public void StartGame()
    {
        System.out.println("User 1, please enter your name:");
        String user1name = Input.next();
        System.out.println("Hello " + user1name + ", please enter your color: (red/black)");
        String user1color = Input.next();
        String user2color;

        if(user1color.equals("r") || user1color.equals("red"))
        {
            System.out.println(user1name + ", your color will be red");
            User1 = new Player(user1name, ECheckersColor.red);
            user2color = "black";
        }
        else
        {
            System.out.println(user1name + ", your color will be black");
            User1 = new Player(user1name, ECheckersColor.black);
            user2color = "red";
        }

        System.out.println("User 2, please enter your name:");
        String user2name = Input.next();
        System.out.println("Hello " + user2name + ", your color will be " + user2color + " because I dislike you.");
        System.out.println("Lets begin...");

        try
        {
            Thread.sleep(1000);
        }
        catch(Exception e)
        {
            //nope nope nope! Why cant you be more like C#, Java? I feel like you're the retarded child of C++, and C# is the
            //better child, making you look even more like a mess. Like at least fix this BS pls.
        }


    }

    private void GameLoop()
    {
        //We're using a simple infinite loop because we can break out of it easily and without
        //issues like furthur execution that we don't want
        while(true)
        {

            //Ask for player move here

            //check if move is valid

            //check if move creates a king piece or if any pieces are collected by the player

            //check if game ends. if so, break and call func.

            //ask for player 2 move

            //repeat
        }
    }


}