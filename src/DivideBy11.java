//DivideBy11

import java.util.*;

/*
Author: Jeremy L.
10/13/17
Simple divide by 11 program, which divides a number by 11 only if it is divisible by 11.
Uses the modulus operator and thats basically it.
*/
public class DivideBy11 extends RunnableProgram
{
    public void Run(Scanner Input)
    {
        System.out.println("Please enter a number to divide:");
        int Num = Input.nextInt();
        
        if((Num % 11) == 0)
            System.out.println("Result: " + (Num / 11));
        else
            System.out.println("Number is not divisible by 11.");
    }
    
    public String GetProgramName()
    {
        return "Divide by 11";
    }
}