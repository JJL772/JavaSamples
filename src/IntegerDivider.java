import java.util.Scanner;

/**
 * 9/25/17
 * Author: Jeremy L.
 *
 * Implements an simple integer divider, which
 * uses only integers, yet will output floating point numbers.
 * Implemented by emulating various types of a float, such as the sign,
 * exponent and fraction.
 * NOTE: Longs are identical to integers, except their max value is 2^63 instead of 2^31,
 * and because we are going to be possibly doing some big math operations, its a good idea to
 * use
 * NOTE: I use javadoc comments when I want to.
 * NOTE: There are no checks for the type of value entered or for out of range numbers, so try to
 * use relatively small numbers ie. less than 2^25
 */
public class IntegerDivider extends RunnableProgram
{
    public void Run(Scanner input)
    {
        int Dividend = 1;
        int Divisor = 1;

        /**
         * Get the users input
         */
        System.out.println("Please enter a dividend:");
        if(input.hasNextInt())
            Dividend = input.nextInt();
        else
            System.out.println("That is not an integer >:(");

        System.out.println("Please enter a divisor");
        if(input.hasNextInt())
            Divisor = input.nextInt();
        else
            System.out.println("That is not an integer >:(");

        //Check for possible divide by zero exception cause
        if(Divisor == 0) {
            System.out.println("Divide by zero error!");
        }

        /**
         * Its important to note that floats are represented in scientific notation in binary
         * We will use this info to get the sign of the number
         * Here's a list of bits in a float number:
         * * bit 31 - Sign (Pos or neg)
         * * bit 23-30 - Exponent
         * * bit 0-22 - Fraction
         * Here's how it would look in scientific notation where Fraction = F, Exponent = E, and
         * Sign = s
         * (-1)^s x Fraction x 10 ^ E
         * For example, if the sign = 1, Fraction is 0.332 and Exponent = 3 then:
         * (-1)^1 x 3.32 x 10 ^ 3
         * Which would simplify to:
         * -1 x 3.32 x 10 ^ 3
         * And also down to:
         * -3.32x10^3
         * and into a normal float:
         * -3320.0
         */

        /**
         * First we should extract the sign on the integer. This way we can determine if the number
         * is positive of negative. We will do this with bit shifts.
         */
        int Sign;

        //These lines are used to determine what the sign is at the beginning of the number. You can use other methods,
        //but bitwise operations are fun
        int DivisorS = (Divisor >> 31) & 0x1;
        int DividendS = (Dividend >> 31) & 0x1;

        // 1 / 1 == 1 and -1 / -1 == 1 If either of these are true, we should have a positive result
        if((DivisorS == 0 && DividendS == 0) || (DivisorS == 1 && DividendS == 1))
            Sign = 1;
        else
            Sign = -1;
        System.out.println(Sign);

        /**
         * Now that we have the sign in the bag, we should take the absolute value of the dividend and the
         * divisor, we should ignore all the stupid signs
         */
        Dividend = Math.abs(Dividend);
        Divisor = Math.abs(Divisor);

        /**
         * Now we need to determine what our non-fractional value is. For example, if we divide 1 by 2
         * we get 0.5, which has no non-fractional portion, as it equals 0. If we divide 1 by 1, our non-fractional value is
         * just 1 because the result is 1.0
         */
        long Integer = Dividend / Divisor;

        /**
         * Now this is the complex part. We need to evaluate the fractional part of the floating point number.
         * I will comment the living hell out of this part.
         */

        /**
         * This is the modulus operator, which returns the remainder of a division operation.
         */
        long Remainder = Dividend % Divisor;

        //For now, this will just be the dividend and divisor. Note that we only multiply the remainder by 1000
        long Fraction = (Remainder * 10000) / (Divisor);

        //The sign is what the integer portion must be multiplied by. This will either be
        //-1 or 1, resulting in -Integer or +Integer
        System.out.println("Result: " + (Sign * Integer) + "." + Fraction);
        System.out.println();
    }
    
    public String GetProgramName()
    {
        return "Integer Divider";
    }
}
