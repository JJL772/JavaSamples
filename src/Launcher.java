import java.util.*;

public class Launcher
{
    private static Scanner Input = null;
    private static RunnableProgram LastProgram = null;
    
    public static void main(String[] args)
    {
        System.out.println("*********************");
        System.out.println("Java Samples Launcher");
        System.out.println("*********************");
        System.out.println("");
        
        Input = new Scanner(System.in);
        
        while(true)
        {
            System.out.flush();
            while(LastProgram != null)
            {
                System.out.println("Would you like to run the program " + LastProgram.GetProgramName() + " ? (y/n)");
                if(Input.next().toLowerCase() == "y")
                    LastProgram.Run(Input);
                else
                {
                    LastProgram = null;
                    break;
                }
            }
            
            System.out.flush();
            
            System.out.println("******************************");
            System.out.println("Please enter a program to run:");
            System.out.println("******************************");
            System.out.println("Valid options are:");
            System.out.println("******************************");
            System.out.println("IntegerDivider");
            System.out.println("DivideBy11");
            System.out.println("******************************");
            
            String in = Input.next();
            
            switch(in.toLowerCase())
            {
                case "integerdivider":
                    LastProgram = new IntegerDivider();
                    System.out.flush();
                    LastProgram.Run(Input);
                    break;
                case "divideby11":
                    LastProgram = new DivideBy11();
                    System.out.flush();
                    LastProgram.Run(Input);
                    break;
                case "bitoperations":
                    LastProgram = new BitOperations();
                    System.out.flush();
                    LastProgram.Run(Input);
                    break;
                default:
                    LastProgram = null;
                    System.out.println("That is an invalid program!");
                    break;
            }
            Threa.sleep(500);
        }
        
    }
}