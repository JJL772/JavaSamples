
/*

Base class for all programs, used to store instances of programs. Use is slightly conveluted and annoying..
Author: Jeremy L.
10/13/17
8:34AM
*/
public class RunnableProgram
{
    public void Run(Scanner input)
    {
        //Nothing. No default behaviour.
    }
    
    public String GetProgramName()
    {
        return "DefaultRunnableProgram";
    }
}